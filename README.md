npm install -g lite-server                                  => Instalando servidor para teste

lite-server                                                 => Para rodar o servidor

php artisan serve --port=8080                               => Roda o servidor laravel em outra porta

Barryvdh\Cors\ServiceProvider::class,                       => Colocar em Config/app.php

composer require barryvdh/laravel-cors                      => Instalando o Cors

php artisan vendor:publish="Barryvdh\Cors\ServiceProvider"  => Criar um arquivo cors.php em config/